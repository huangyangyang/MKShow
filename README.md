# MKShow
这是一个自定义的弹出框
# 自定义的控件
MarkShow
这是一个比较粗糙的弹出框目前离我的设想还有很大的距离,作者会不断的优化和增加功能中
这里是基本的使用方法 使用之前先检查有没有导入Masnory

本公开库已经支持cocopod pod地址为 https://github.com/jxcz94754/MKShow.git

pod 方式

pod 'MKShow', :git => 'https://github.com/jxcz94754/MKShow.git'

首先看下我们的使用方式  我是直接通过MKShowDynamic类方法来实现的 你也可以尝试实例化这个对象
```swift
//设置默认的颜色 （这里是设置弹出框的基础颜色）
[MKShowDynamic setDefaultColor:NAVBAR_TINTCOLOR];
//设置弹出框的背景图片
[MKShowDynamic setDefaultBackImage:[UIImage imageNamed:@"ico_show_black"]];
//设置默认的弹出样式
[MKShowDynamic setDefaultMKShowShowStyle:MKShowShowStyleDefault];
//设置默认的动画样式
[MKShowDynamic setDefaultMKShowDynamicStyle:MKShowDynamicStyleDefault];
//基本的弹出样式
[MKShowDynamic showMkViewWithMessage:@"消息提示" sureCallBack:^(id backObject) {

} cancelCallBack:^(id backObject) {

}];

//注意点 默认样式一旦发生改变那么全局的都会变 如果你在一个项目当中要使用不同的弹出形式 建议每一个弹框 都要设置下 弹出样式和动画样式

```

看下我们设置弹出框基本样式的类MKShow,其中做了一些弹出动画和弹出样式的枚举方式 这只是一个枚举类供全局使用
```swift
typedef NS_ENUM(NSUInteger, MKShowDynamicStyle) {
// Various style according to animation
    MKShowDynamicStyleDefault, //默认的显示状态 显示弹出框 The default display status display pop-up box
    MKShowDynamicStyleBottomUp, // 从底部飞出的弹出框 Flying out of the bottom of the pop-up box
    MKShowDynamicStyleLeftIn, //显示一个从左侧进入的弹出框 Display a pop-up box from the left to enter
    MKShowDynamicStyleRightIn, //显示一个从右侧进入的弹出框 To display a pop-up box from the right to enter
    MKShowDynamicStyleCenter, //显示一个从中心点进入的弹出框带有动画 Display a pop-up box into the center with animation
};

typedef NS_ENUM(NSUInteger, MKShowShowStyle) {
    // Various style according to animation
    MKShowShowStyleDefault, //默认的显示状态 显示弹出框 The default display status display pop-up box
    MKShowShowStyleShadow, //带有阴影的显示状态 Display status with shadow
    MKShowShowStyleMultipleButton,//带有多个按钮的 Display With multiple buttons
};
```

MKShowk类中,全局的按钮的高亮显示的方法,与枚举对应的动画形式
```swift
@interface MKShow : NSObject

#pragma mark - 自有类方法 (不可设置的方法)

//获取一张带有颜色的图片 主要作用于点击时候的高亮显示
//Get a picture with color A major role in the click when highlighted
+ (UIImage *)imageWithColor:(UIColor *)color;

//界面弹出的动画 主要用户弹框的显示 从中心点进入的弹出框
//Interface pop-up box animation major users of display from the center into the pop-up box
+ (void)showDynamicStyleCenter:(UIView *)view;

//界面弹出的动画 主要用户弹框的显示 从底部飞出的弹出框
//Popup animation interface The main user display box Flying out of the bottom of the pop-up box
+ (void)showDynamicStyleBottomUp:(UIView *)view;

//界面弹出的动画 主要用户弹框的显示 一个从左侧进入的弹出框
+ (void)showDynamicStyleLeftIn:(UIView *)view;

//界面弹出的动画 主要用户弹框的显示 一个从右侧进入的弹出框
//Interface popup animation mainly users play box displays a pop-up box from the left to enter
+ (void)showDynamicStyleRightIn:(UIView *)view;

@end

```
在一个是我们调用类MKShowDynamic 本类开放了一些可供设置的类方法和属性,你可以通过这些来修改弹出界面的相关显示

```swift

@interface MKShowDynamic : NSObject

//初始化方法
SINGLETON_INTERFACE

//设置初始的显示方式 Set the initial display mode
+ (void)setDefaultMKShowDynamicStyle:(MKShowDynamicStyle)showDynamicStyle;

//设置是否有阴影的显示方式 Set whether there is a shadow display mode
+ (void)setDefaultMKShowShowStyle:(MKShowShowStyle)showShowStyle;

//设置背景图片 Set the background image
+ (void)setDefaultBackImage:(UIImage *_Nullable)image;

//设置统一颜色(包括线条 按钮) Unified color (including line button)
+ (void)setDefaultColor:(UIColor *_Nullable)color;

//设置确认按钮的颜色 Confirm the color of the button
+ (void)setSureColor:(UIColor *_Nullable)sureColor;

//设置取消按钮的颜色 The color of the set the cancel button
+ (void)setCancelColor:(UIColor *_Nullable)cancelColor;

/**
展示一个有消息提示的弹出框并且有带block返回

@param message message                         //消息提示
@param sureCallBack  sureCallBack              //点击确认的返回
@param cancelCallBack cancelCallBack           //点击取消的返回
*/

+ (void)showMkViewWithMessage:(NSString *_Nullable)message
                sureCallBack:(void(^_Nullable)(id _Nullable backObject))sureCallBack
                cancelCallBack:(void(^_Nullable)(id _Nullable backObject))cancelCallBack;

/**
展示一个有消息提示并且有文本的弹出框并且有带block返回

@param message message                         //消息提示
@param text text                               //显示的文本内容
@param sureCallBack sureCallBack               //点击确认的返回
@param cancelCallBack cancelCallBack           //点击取消的返回
*/

+ (void)showMkViewWithMessage:(NSString *_Nullable)message Text:(NSString *_Nullable)text
                sureCallBack:(void(^_Nullable)(id _Nullable backObject))sureCallBack
                cancelCallBack:(void(^_Nullable)(id _Nullable backObject))cancelCallBack;


/**
展示一个有消息提示并且可以输入弹出框并且有带block返回

@param message message                         //消息提示
@param sureCallBack sureCallBack               //点击确认的返回 （返回的是number类型 按钮的下标）
@param cancelCallBack cancelCallBack           //点击取消的返回 （该方法没有取消的返回）
*/

+ (void)showMkInPutViewWithMessage:(NSString *_Nullable)message
                        sureCallBack:(void(^_Nullable)(id _Nullable backObject))sureCallBack
                        cancelCallBack:(void(^_Nullable)(id _Nullable backObject))cancelCallBack;

/**
展示一个有消息提示并且带多个按钮的弹出框并且有带block返回

@param message message
@param titleArray titleArray
@param sureCallBack sureCallBack
@param cancelCallBack cancelCallBack
*/
+ (void)showMultipleButtonViewWithMessage:(NSString *_Nullable)message
                            titleArray:(NSArray *_Nullable)titleArray
                        sureCallBack:(void(^_Nullable)(id _Nullable backObject))sureCallBack
                        cancelCallBack:(void(^_Nullable)(id _Nullable backObject))cancelCallBack;



//**下面是一些基本的属性设置你可以用这个修改你要修改的属性你也可以使用上面的类方法来进行界面的初始设置
/*Here are some basic properties of Settings you can use the change you want to modify the properties of the you can also use the above class methods for initial setup of the interface
*/

//设置显示的方式的属性 Set properties show in the way
@property (assign, nonatomic,setter = setDefaultShowStyle:) MKShowDynamicStyle showDynamicStyle;

//设置是否有阴影的显示方式 Set whether there is a shadow display mode
@property (assign, nonatomic,setter = setDefaultShadowStyle:) MKShowShowStyle showShowStyle;

//设置弹出框的背景颜色 Set the background color of the pop-up box
@property (strong, nonatomic,setter = setBackImage:) UIImage * _Nullable image;

//设置统一颜色(包括线条 按钮) Unified color (including line button)
@property (strong, nonatomic,setter = setTintColor:) UIColor * _Nullable defaultColor;

//设置确认按钮的颜色 Confirm the color of the button
@property (strong, nonatomic, setter = setSureButtonColor:) UIColor * _Nullable sureColor;

//设置取消按钮的颜色 The color of the set the cancel button
@property (strong, nonatomic, setter = setCancelButtonColor:) UIColor * _Nullable cancelColor;

@end

```

新增多输入框的弹出框 使用方法与多按钮类似 支持动画和阴影


转载或下载帮忙评论分享 https://gitee.com/huangyangyang/MKShow
